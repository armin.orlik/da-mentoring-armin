output "ecs_lb_endpoint" {
  value = aws_lb.default.dns_name
}
output "website_endpoint" {
  value = aws_s3_bucket_website_configuration.mybucketconfig.website_endpoint
}

