#S3 bucket for my website
resource "aws_s3_bucket" "mybucket" {
  bucket = var.s3_bucket
  policy = templatefile("s3-policy.json", { bucket = var.s3_bucket })

}
resource "aws_s3_bucket_acl" "mybucket" {
  bucket = var.s3_bucket
  acl    = "public-read"
}


#Uploading website files into a bucket
resource "aws_s3_bucket_object" "html" {
  for_each     = fileset("./html/", "**/*.html")
  bucket       = var.s3_bucket
  key          = each.value
  source       = "./html/${each.value}"
  content_type = "text/html"
  depends_on = [
    aws_s3_bucket.mybucket
  ]
}

resource "aws_s3_bucket_website_configuration" "mybucketconfig" {
  bucket = var.s3_bucket

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "error.html"
  }

  routing_rule {
    condition {
      key_prefix_equals = "docs/"
    }
    redirect {
      replace_key_prefix_with = "documents/"
    }
  }
}

#S3 bucket to forward non-www to www
resource "aws_s3_bucket" "mybucket_non_www" {
  bucket = "www.${var.s3_bucket}"
  policy = templatefile("s3-policy.json", { bucket = "www.${var.s3_bucket}" })

}

resource "aws_s3_bucket_website_configuration" "mybucket_non_www" {
  bucket = "www.${var.s3_bucket}"
  redirect_all_requests_to {
    host_name = "https://www.${var.s3_bucket}"
  }
}
resource "aws_s3_bucket_acl" "mybucket_non_www" {
  bucket = "www.${var.s3_bucket}"
  acl    = "public-read"
}