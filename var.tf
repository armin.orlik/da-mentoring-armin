variable "s3_bucket" {
  type        = string
  description = "The name of the bucket"
}