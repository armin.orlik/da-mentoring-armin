terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }

  required_version = ">= 0.12.31"
}

provider "aws" {
  profile = "da_tf"
  region  = "eu-west-1"


  default_tags {
    tags = {
      Name = "armin-mentoring"
    }
  }
}

variable "app_count" {
  type    = number
  default = 1
}

